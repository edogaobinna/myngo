package com.notitia.bwsapp.util

class Case(_id:Long?,_ca15:String?, _ca125:String?,_ca25:String?,_antigen:String?, _psa:String?) {
    val id = _id
    val ca15 = _ca15
    val ca125 = _ca125
    val ca25 = _ca25
    val antigen = _antigen
    val psa = _psa
}