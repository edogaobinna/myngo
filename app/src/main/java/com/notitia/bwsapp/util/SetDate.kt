package com.notitia.bwsapp.util

import android.app.DatePickerDialog
import android.content.Context
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Spinner
import java.text.SimpleDateFormat
import java.util.*


internal class SetDate(private val editText: EditText, view: View?, private val ctx: Context) : OnFocusChangeListener,
    DatePickerDialog.OnDateSetListener,View.OnClickListener {

    private val sdf: SimpleDateFormat
    private val myCalendar: Calendar
    private val views:View?

    init {
        this.editText.setOnClickListener(this)
        this.editText.onFocusChangeListener = this
        this.myCalendar = Calendar.getInstance()
        this.sdf = SimpleDateFormat(myFormat,Locale.UK)
        this.views = view
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, month)
        myCalendar.set(Calendar.DAY_OF_MONTH, day)
        this.editText.setText(sdf.format(myCalendar.time))

        if(this.views!=null){
            if(myCalendar.before(Calendar.getInstance())){
                if(this.views is EditText){
                    this.views.show()
                }
                if(this.views is Spinner){
                    this.views.show()
                }
                if(this.views is CheckBox){
                    this.views.show()
                }
            }else{
                if(this.views is EditText){
                    this.views.hide()
                }
                if(this.views is Spinner){
                    this.views.hide()
                }
                if(this.views is CheckBox){
                    this.views.hide()
                }
            }
        }
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            val year = myCalendar.get(Calendar.YEAR)
            val month = myCalendar.get(Calendar.MONTH)
            val dayofMonth = myCalendar.get(Calendar.DAY_OF_MONTH)
            DatePickerDialog(this.ctx,this,year,month,dayofMonth).show()
        }
    }

    fun getCalendar():Calendar{
        return myCalendar
    }

    override fun onClick(v: View?) {
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val dayofMonth = myCalendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(this.ctx,this,year,month,dayofMonth).show()
    }

}