package com.notitia.bwsapp.util

import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class GenericPagerAdapter(fm:FragmentManager): FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var fragmentList: ArrayList<Fragment>? = null
    private var fragmentTitleList: ArrayList<String>? = null

    init {
        fragmentList = ArrayList()
        fragmentTitleList = ArrayList()
    }

    override fun getItem(position: Int): Fragment {
        return fragmentList!![position]
    }

    override fun getCount(): Int {
        return fragmentList!!.size
    }
    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitleList!![position]
    }

    fun addFragment(fragment: Fragment?, title: String?) {
        fragmentList!!.add(fragment!!)
        fragmentTitleList!!.add(title!!)
    }
}