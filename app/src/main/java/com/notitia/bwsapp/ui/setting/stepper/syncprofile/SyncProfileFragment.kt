package com.notitia.bwsapp.ui.setting.stepper.syncprofile

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.SyncProfileFragmentBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.sync_profile_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class SyncProfileFragment : Fragment(),BlockingStep,KodeinAware,SetListener {

    private lateinit var binding: SyncProfileFragmentBinding

    override val kodein by kodein()

    private val factory: SyncProfileViewModelFactory by instance<SyncProfileViewModelFactory>()

    var count:Int = 0
    var complete:Boolean = false

    companion object {
        fun newInstance() = SyncProfileFragment()
    }

    private lateinit var viewModel: SyncProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.sync_profile_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(SyncProfileViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        viewModel.setListener = this

        viewModel.count.observe(viewLifecycleOwner, Observer {
            count = it
        })
        return binding.root
    }

    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback?) {
        callback?.goToPrevStep()
    }

    override fun onSelected() {
        return
    }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback?) {
        if(complete){
            callback?.stepperLayout?.showProgress("Please Wait")
            Handler().postDelayed({
                callback?.goToNextStep()
                callback?.stepperLayout?.hideProgress()
            },3000)

        }else{
            Toast.makeText(context,"Wait while download completes", Toast.LENGTH_LONG).show()
        }
    }

    override fun verifyStep(): VerificationError? {
        return null
    }

    override fun onError(error: VerificationError) {
        Toast.makeText(context,error.errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onStarted(id: Int) {
        if(id==1){
            check.text = "Downloading all profile records"
            check.show()
            note.hide()
            create.hide()
            progress_bar.show()
            viewModel.profileSync()
        }
    }

    override fun onSuccess(message: String) {
        Toast.makeText(context,message, Toast.LENGTH_LONG).show()
        check.text = "Profile download completed click next to continue"
        check.show()
        progress_bar.hide()
        complete = true
    }

    override fun onError(message: String) {
        Toast.makeText(context," No Profile to download, click next to continue", Toast.LENGTH_LONG).show()
        check.text = "Click next to continue"
        check.show()
        progress_bar.hide()
        complete = true
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            if((it as QuerySnapshot).isEmpty ) {
                onError("Patient Data is Empty")
            }else{
                viewModel.processData(it)
            }
        })
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
