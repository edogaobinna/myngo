package com.notitia.bwsapp.ui.outreach

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

class OutreachViewModelFactory (
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return OutreachViewModel(repository,prefs) as T
        }
}