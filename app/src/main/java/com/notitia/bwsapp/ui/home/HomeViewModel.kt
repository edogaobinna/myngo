package com.notitia.bwsapp.ui.home

import android.view.View
import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.Category
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.Coroutines
import org.jetbrains.anko.doAsync
import java.util.*

class HomeViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    val outreach = repository.getOutreachCount()
    val profile = repository.getProfileCount()
    val male = repository.getMaleCount()
    val female = repository.getFemaleCount()
    val user = repository.getAuth()

    val incomeCategory:Array<String> = arrayOf("Intervention Fund", "Grant", "Sponsor","Investment","Donation","Loan","Other")
    val expenseCategory:Array<String> = arrayOf("Salary","Intervention","Office Supplies","Utility Bills","Miscellaneous","Other")

    var  homeListener:HomeListener? = null

    fun onProfileClick(view: View){
        homeListener?.onSuccess(1)
    }
    fun onOutreachClick(view: View){
        homeListener?.onSuccess(2)
    }

    fun insertIncome(){
        doAsync {
            for(e in incomeCategory.indices){
                val cate = Category(null, incomeCategory[e],"1","")
                Coroutines.main {
                    repository.checkSaveCate(cate)
                }
            }
            for(e in expenseCategory.indices){
                val cate = Category(null, expenseCategory[e],"2","")
                Coroutines.main {
                    repository.checkSaveCate(cate)
                }
            }
        }

    }
}