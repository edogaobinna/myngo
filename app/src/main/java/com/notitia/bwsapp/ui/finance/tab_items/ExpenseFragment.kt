package com.notitia.bwsapp.ui.finance.tab_items

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.ExpenseAdapter
import com.notitia.bwsapp.data.db.entities.Expense
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.databinding.ExpenseFragmentBinding
import com.notitia.bwsapp.util.*
import kotlinx.android.synthetic.main.expense_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ExpenseFragment : Fragment(),KodeinAware,TabListener {

    private lateinit var binding: ExpenseFragmentBinding

    override val kodein by kodein()

    private val factory: IncomeViewModelFactory by instance()
    private val repository:DataRepository by instance()

    private var listPatient:ArrayList<Expense>? = ArrayList()
    private var incomeAdapter: ExpenseAdapter? = ExpenseAdapter(listPatient)
    var years = ArrayList<String>()

    companion object {
        fun newInstance() = ExpenseFragment()
    }

    private lateinit var viewModel: IncomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.expense_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this,factory).get(IncomeViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel.tabListener = this

        viewModel.expenses.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                incomeAdapter?.notifyDataSetChanged()
                emptyState.hide()
                recycler_view.show()
            }else{
                emptyState.show()
                recycler_view.hide()
            }
        })

        viewModel.incomeCate.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()) {
                years = viewModel.getIncome(it)
            }
        })

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = incomeAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    recycler_view,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]
                                Utility().editExpenseDialog(context!!,repository,patient,years)
                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
    }

    override fun onStarted(repository: DataRepository) {
        Utility().expenseDialog(context!!,repository,years)
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
