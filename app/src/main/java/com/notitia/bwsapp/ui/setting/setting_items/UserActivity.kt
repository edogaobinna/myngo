package com.notitia.bwsapp.ui.setting.setting_items

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.ActivityUserBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.ui.setting.SettingViewModel
import com.notitia.bwsapp.ui.setting.SettingViewModelFactory
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.notitia.bwsapp.util.snackbar
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_user.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class UserActivity : AppCompatActivity(),KodeinAware,SetListener,AdapterView.OnItemSelectedListener {

    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Add User"

        val binding: ActivityUserBinding = DataBindingUtil.setContentView(this,R.layout.activity_user)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        binding.role.onItemSelectedListener = this

        viewmodel.setListener = this
    }

    override fun onStarted(id: Int) {
        create.hide()
        progress_bar.show()
    }

    override fun onSuccess(message: String) {
        root_layout.snackbar(message)
        create.show()
        progress_bar.hide()
    }

    override fun onError(message: String) {
        root_layout.snackbar(message)
        create.show()
        progress_bar.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            if(it as Boolean){
                onSuccess("User Account Created Successfully")
            }else{
                onError("Cannot create users at this moment")
            }
        })
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.role->if(!parent.selectedItem.toString().equals("role",ignoreCase = true))
        {viewmodel.role = parent.selectedItem.toString()}
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
