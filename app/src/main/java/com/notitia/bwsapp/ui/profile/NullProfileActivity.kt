package com.notitia.bwsapp.ui.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.ProfileAdapter
import com.notitia.bwsapp.data.db.entities.Profile
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityProfileBinding
import com.notitia.bwsapp.util.*
import kotlinx.android.synthetic.main.activity_profile.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class NullProfileActivity : AppCompatActivity(),KodeinAware,ProfileListener {
    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: ProfileViewModel

    private var listPatient:ArrayList<Profile>? = ArrayList()
    private var profileAdapter: ProfileAdapter? = ProfileAdapter(listPatient)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "No Outreach Profile"
        val binding: ActivityProfileBinding = DataBindingUtil.setContentView(this,R.layout.activity_profile)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.profileListener = this

        viewmodel.nullProfile.observe(this, Observer {
            if(it.isNotEmpty()){
                toast(it.size.toString())
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                profileAdapter?.notifyDataSetChanged()
                emptyState.hide()
                recycler_view.show()
            }else{
                emptyState.show()
                recycler_view.hide()
            }
        })

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = profileAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    recycler_view,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]
                                prefs.savePreference(UniqueId, patient.uniqueId)
                                Intent(context, ProfileDetailActivity::class.java).also {
                                    startActivity(it)
                                }
                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        /*if(id==R.id.export){
            Intent(this,AddProfileActivity::class.java).also {
                startActivity(it)

            }
            return true
        }*/
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        Intent(this,AddProfileActivity::class.java).also {
            startActivity(it)
        }
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        TODO("Not yet implemented")
    }
}
