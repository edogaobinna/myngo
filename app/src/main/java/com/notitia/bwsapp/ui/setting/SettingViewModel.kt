package com.notitia.bwsapp.ui.setting

import android.content.Context
import android.os.Environment
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.notitia.bwsapp.data.db.entities.*
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.*
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class SettingViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel(){

    val profiles = repository.getProfile()
    val medicals = repository.getMedicals()
    val outreach = repository.getOutreach()
    val screening = repository.getScreening()
    var listUser:ArrayList<Auth>? = ArrayList()
    var fullname:String? = null
    var employeeId:String? = null
    var username:String? = null
    var role:String? = null
    var password:String?=null
    var cpassword:String? = null
    var setListener:SetListener? = null

    private val _role = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(roleId)
    }

    val knowRole: LiveData<String?> = _role

    fun onCreateUser(view: View){
        setListener?.onStarted(1)
    }

    fun onViewUser(view: View){
        setListener?.onStarted(2)
    }
    fun onUpdateClick(view: View){
        setListener?.onStarted(3)
    }
    fun onSyncClick(view: View){
        setListener?.onStarted(4)
        val response = Firebase().getDocument(Profile)
        setListener?.onFirebaseFinish(response,2)
    }
    fun signoutUser(view: View){
        val response = Firebase().signOut()
        setListener?.onFirebaseFinish(response,1)
        //setListener?.onStarted(5)
    }
    fun downloadApp(view: View){
        setListener?.onStarted(2)
    }
    fun onDownload(view: View){
        setListener?.onStarted(5)
    }
    fun onPatientDownload(view: View){
        setListener?.onStarted(1)
    }
    fun onMedicalDownload(view: View){
        setListener?.onStarted(2)
    }
    fun onOutreachDownload(view: View){
        setListener?.onStarted(3)
    }
    fun onScreeningDownload(view: View){
        setListener?.onStarted(4)
    }

    fun onCreateClicked(view:View){
        setListener?.onStarted(0)
        val user = HashMap<String, Any?>()
        user[name] = fullname
        user[employee] = employeeId
        user[user_name] = username
        user[roleId] = role
        user[activated] = true
        if(fullname.isNullOrEmpty() || employeeId.isNullOrEmpty() || username.isNullOrEmpty() || role.isNullOrEmpty() ||
            password.isNullOrEmpty() || cpassword.isNullOrEmpty()){
            setListener?.onError("Cannot submit with empty fields")
            return
        }
        if(!password.equals(cpassword,true)){
            setListener?.onError("Password Doesn't Match")
            return
        }
        val response = Firebase().createUser(username!!,password!!,user)
        setListener?.onFirebaseFinish(response,0)
    }

    fun getUsers(){
        val response = Firebase().getDocument(Users)
        setListener?.onFirebaseFinish(response,0)
    }

    fun getUpdate(){
        val response = Firebase().getDocument(Update)
        setListener?.onFirebaseFinish(response,0)
    }

    fun processUserList(data:QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val auth =
                    Auth(1,
                        obj.getString(name),
                        obj.getString(employee),
                        obj.getString(user_name),
                        Date(Calendar.getInstance().timeInMillis),
                        obj.getString(roleId),
                        obj.getBoolean(activated))
                listUser?.add(auth)
            }
            setListener?.onUserProcess(listUser)
        }else{
            setListener?.onError("")
        }
    }
    fun processUpdate(data:QuerySnapshot){
        if(!data.isEmpty) {
            var auth:String = ""
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                auth = obj.getString("Version_Id")
                prefs.savePreference("url",obj.getString("Url"))
            }
            if(prefs.getLastSavedAt(Version).equals(auth,true)) {
                setListener?.onStarted(1)
            }else{
                setListener?.onSuccess(auth)
            }
        }else{
            setListener?.onError("")
        }
    }

    fun clearPref(){
        prefs.deleteAll()
    }

    fun processProfiles(data: QuerySnapshot){
        try {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Profile(
                            null,
                            obj.getString(UniqueId),
                            obj.getString(Surname),
                            obj.getString(firstName),
                            obj.getString(Age),
                            obj.getString(Gender),
                            obj.optString(Parity),
                            obj.getString(PhoneNumber),
                            obj.optString(Address),
                            obj.optString(Tribe),
                            obj.getString(Nationality),
                            obj.optString(
                                OutreachId
                            ),
                            Converter().timeStamptoDate(obj.getString(DateCreated)),
                            obj.getBoolean(Active)
                        )
                    Coroutines.main {
                        repository.checkSaveProfile(profile)
                    }
                }
                val response = Firebase().getDocument(Medical)
                setListener?.onFirebaseFinish(response,3)
            } else {
                setListener?.onError("An Error Occurred.Try again later")
            }
        }catch (e:Exception){

        }
    }
    fun processMedical(data: QuerySnapshot){
        try {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        com.notitia.bwsapp.data.db.entities.Medical(
                            null, obj.getString(ProfileId), obj.getString(UniqueId), obj.optString(
                                Systolic
                            ), obj.optString(Diastolic), obj.optString(BloodSugar), obj.optString(
                                Height
                            ), obj.optString(Weight), obj.optString(BMI)
                        )
                    Coroutines.main {
                        repository.checkSaveMedical(profile)
                    }
                }
                val response = Firebase().getDocument(Screening)
                setListener?.onFirebaseFinish(response,4)
            } else {
                setListener?.onError("An Error Occurred.Try again later")
            }
        }catch (e:Exception){

        }
    }
    fun processScreening(data: QuerySnapshot){
        try {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        com.notitia.bwsapp.data.db.entities.Screening(
                            null,
                            obj.getString(ProfileId),
                            obj.getString(UniqueId),
                            obj.optString(
                                TestType
                            ),
                            obj.optString(TestResult),
                            Converter().timeStamptoDate(obj.getString(DateConducted)),
                            obj.optString(VenueConducted),
                            obj.optString(ReportingDoctor)
                        )
                    Coroutines.main {
                        repository.checkSaveScreening(profile)
                    }
                }
                val response = Firebase().getDocument(Test)
                setListener?.onFirebaseFinish(response,5)
            } else {
                setListener?.onError("An Error Occurred.Try again later")
            }
        }catch (e:Exception){

        }
    }
    fun processTest(data: QuerySnapshot){
        try {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Tests(
                            null,obj.getString(ProfileId),obj.getString(UniqueId),obj.optString(
                                PapSmear),
                            com.notitia.bwsapp.data.db.entities.PapReferred(obj.optBoolean(PapReferred),obj.optString(
                                PapDescription)),obj.optString(CryotherapyResult),obj.optString(
                                PsaResult), com.notitia.bwsapp.data.db.entities.PsaReferred(obj.optBoolean(PsaReferred),obj.optString(
                                PsaDescription)),obj.optString(MammographyResult), com.notitia.bwsapp.data.db.entities.MamReferred(obj.optBoolean(
                                MamReferred),obj.optString(MamDescription)),obj.optString(
                                UltrasoundResult), com.notitia.bwsapp.data.db.entities.UltraReferred(obj.optBoolean(UltraReferred),obj.optString(
                                UltraDescription))
                        )
                    Coroutines.main {
                        repository.checkSaveTest(profile)
                    }
                }
                setListener?.onSuccess("Sync Operation Completed")
            } else {
                setListener?.onError("An Error Occurred.Try again later")
            }
        }catch (e:Exception){

        }
    }

    fun exportProfileToCsv(item: List<Profile>,context: Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val file = File.createTempFile("profile", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            //csvWrite.writeNext(curCSV.getColumnNames())
            val arrStr = arrayOf<String?>(
                "Id",
                "UniqueId",
                "Surname","FirstName","Age","Gender","Parity","PhoneNumber","Address","Tribe","Nationality",
                "OutreachId","Date_Created"
            )
            csvWrite.writeNext(arrStr)
            for(e in item.indices){
                val data = arrayOf(
                    item[e].id.toString(),item[e].uniqueId,item[e].surname.toString(),item[e].firstName,
                    item[e].age,item[e].gender,item[e].parity,
                    item[e].phoneNumber,item[e].address,item[e].tribe,
                    item[e].nationality,item[e].outreach_uniqueId,item[e].dateCreated.toString()
                )
                csvWrite.writeNext(data)
            }
            csvWrite.close()
            setListener?.onError("Export suceessful")
            //pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            setListener?.onError("Cannot export records to csv, Contact system administrator")
        }
    }
    fun exportMedicalToCsv(item: List<Medical>,context: Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val file = File.createTempFile("medical", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            //csvWrite.writeNext(curCSV.getColumnNames())
            val arrStr = arrayOf<String?>(
                "Id",
                "UniqueId", "ProfileId","Weight","Height","BodyMassIndex","Systolic","Diastolic","BloodSugar"
            )
            csvWrite.writeNext(arrStr)
            for(e in item.indices){
                val data = arrayOf(
                    item[e].id.toString(),item[e].uniqueId,item[e].profileId.toString(),item[e].weight,
                    item[e].height,item[e].BMI,item[e].systolicBP,
                    item[e].diastolicBP,item[e].bloodGlucose)
                csvWrite.writeNext(data)
            }
            csvWrite.close()
            setListener?.onError("Export suceessful")
            //pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            setListener?.onError("Cannot export records to csv, Contact system administrator")
        }
    }
    fun exportOutreachToCsv(item: List<Outreach>,context: Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val file = File.createTempFile("outreach", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            //csvWrite.writeNext(curCSV.getColumnNames())
            val arrStr = arrayOf<String?>(
                "Id",
                "UniqueId", "Title","Location","Awareness","Male Attendance","Female Attendance","Start Date","End Date",
                "Breast Cancer","Cervical Cancer","Prostate","Ultrasound"
            )
            csvWrite.writeNext(arrStr)
            for(e in item.indices){
                val data = arrayOf(
                    item[e].id.toString(),item[e].uniqueId,item[e].name,item[e].location,item[e].awareness,
                    item[e].maleAttendance,item[e].femaleAttendance,item[e].startDate.toString(),
                    item[e].endDate.toString(),item[e].breastCancer.toString(),item[e].cervicalCancer.toString(),
                    item[e].prostateCancer.toString(),item[e].ultrasound.toString())
                csvWrite.writeNext(data)
            }
            csvWrite.close()
            setListener?.onError("Export suceessful")
            //pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            setListener?.onError("Cannot export records to csv, Contact system administrator")
        }
    }
    fun exportScreeningToCsv(item: List<Screening>,context: Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val file = File.createTempFile("outreach", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            //csvWrite.writeNext(curCSV.getColumnNames())
            val arrStr = arrayOf<String?>(
                "Id",
                "UniqueId", "ProfileId","Test Type","Test Result","Reporting Doctor","Venue Conducted","Date Conducted"
            )
            csvWrite.writeNext(arrStr)
            for(e in item.indices){
                val testTypeArr = item[e].testTypes?.replace("[","")?.replace("]","")?.split(",")
                val testResultArr = item[e].testResults?.replace("[","")?.replace("]","")?.split(",")
                if(testTypeArr?.size==1) {
                    val data = arrayOf(item[e].id.toString(), item[e].uniqueId, item[e].profileId, testTypeArr[0],
                        testResultArr!![0], item[e].doctor, item[e].venueConducted, item[e].dateConducted.toString())
                    csvWrite.writeNext(data)
                }else{
                    val data = arrayOf(item[e].id.toString(), item[e].uniqueId, item[e].profileId, testTypeArr!![0], testResultArr!![0],
                        item[e].doctor, item[e].venueConducted, item[e].dateConducted.toString())
                    csvWrite.writeNext(data)
                    for(i in 1 until testTypeArr.size){
                        val data1:Array<String?>? = arrayOf(" ", " ", " ", testTypeArr[i], testResultArr[i],
                            " ", " ", " ")
                        csvWrite.writeNext(data1)
                    }
                }
            }
            csvWrite.close()
            setListener?.onError("Export suceessful")
            //pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            setListener?.onError("Cannot export records to csv, Contact system administrator")
        }
    }
}