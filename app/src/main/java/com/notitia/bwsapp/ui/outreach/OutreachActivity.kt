package com.notitia.bwsapp.ui.outreach

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.OutreachAdapter
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityOutreachBinding
import com.notitia.bwsapp.util.*
import com.notitia.bwsapp.util.SetDate
import kotlinx.android.synthetic.main.activity_outreach.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class OutreachActivity : AppCompatActivity(),KodeinAware,OutreachListener {

    override val kodein by kodein()

    private val factory: OutreachViewModelFactory by instance<OutreachViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: OutreachViewModel
    private lateinit var binding: ActivityOutreachBinding

    private var sheetBehavior: BottomSheetBehavior<View>? = null
    private var listPatient:ArrayList<Outreach>? = ArrayList()
    private var outreachAdapter: OutreachAdapter? = OutreachAdapter(listPatient)

    private lateinit var myCalendar: Calendar
    private lateinit var myCalendar1: Calendar
    private var alertDialog:AlertDialog.Builder? = null
    private var alert:AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Outreach Programs"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_outreach)
        viewmodel = ViewModelProvider(this,factory).get(OutreachViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.outreachListener = this

        myCalendar = SetDate(binding.included.startDateEdt,null,this).getCalendar()
        myCalendar1 = SetDate(binding.included.endDateEdt,null,this).getCalendar()

        binding.fabAddOutreach.setOnClickListener{
            if (sheetBehavior!!.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior!!.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                sheetBehavior!!.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }
        binding.included.saveOutreachButton.setOnClickListener{
            viewmodel.title = binding.included.nameEdt.text.toString()
            viewmodel.uniqueIdText = binding.included.unique.text.toString()
            viewmodel.location = binding.included.locationEdt.text.toString()
            viewmodel.maleAttendance = binding.included.maleAttendanceEdt.text.toString()
            viewmodel.femaleAttendance = binding.included.femaleAttendanceEdt.text.toString()
            viewmodel.awareness = binding.included.awarenessEdt.text.toString()
            viewmodel.startDate = binding.included.startDateEdt.text.toString()
            viewmodel.endDate = binding.included.endDateEdt.text.toString()
            viewmodel.cervical = binding.included.cervicalCancerRadioBtn.isChecked
            viewmodel.breast = binding.included.breastCancerRadioBtn.isChecked
            viewmodel.prostate = binding.included.prostateRadioBtn.isChecked
            viewmodel.utrasound = binding.included.ultrasoundRadioBtn.isChecked
            if(viewmodel.createOutreach(myCalendar,myCalendar1)) {
                clearAllFields()
                sheetBehavior!!.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }else{
                toast("Cannot submit empty fields")
            }
        }

        sheetBehavior = BottomSheetBehavior.from(binding.included.sheetBottom)
        sheetBehavior!!.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })

        viewmodel.outreach.observe(this, androidx.lifecycle.Observer {
            if(it.isNotEmpty()){
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                outreachAdapter?.notifyDataSetChanged()
                emptyState.hide()
                recycler_view.show()
            }else{
                emptyState.show()
                recycler_view.hide()
            }
        })

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = outreachAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    recycler_view,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]
                                prefs.savePreference(Outreach, patient.id.toString())
                                Intent(context, EditOutreachActivity::class.java).also {
                                    startActivity(it)
                                }
                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.ic_menu_outreach, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        if(id == R.id.action_outreach){
            viewmodel.outreachSync()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        val view = LayoutInflater.from(this).inflate(R.layout.get_outreach, null) as View
        alertDialog = AlertDialog.Builder(this)
        alertDialog?.setView(view)
        alertDialog?.setCancelable(false)
        alert = alertDialog?.create()
        alert?.show()
    }

    override fun onSuccess(message: String) {
        alert?.dismiss()
        toast("Download Complete")
    }

    override fun onError(message: String) {
        alert?.dismiss()
        toast("An error occurred. Please try again!!")
    }

    override fun onFirebaseFinish(response: LiveData<Any>) {
        response.observe(this, androidx.lifecycle.Observer {
            if((it as QuerySnapshot).isEmpty) {
                onError("Outreach Data is Empty")
            }else{
                viewmodel.processOutreach(it)
            }
        })
    }

    private fun clearAllFields(){
        binding.included.nameEdt.text.clear()
        binding.included.locationEdt.text.clear()
        binding.included.maleAttendanceEdt.text.clear()
        binding.included.femaleAttendanceEdt.text.clear()
        binding.included.awarenessEdt.text.clear()
        binding.included.startDateEdt.text.clear()
        binding.included.endDateEdt.text.clear()
        binding.included.cervicalCancerRadioBtn.isChecked = false
        binding.included.breastCancerRadioBtn.isChecked = false
        binding.included.prostateRadioBtn.isChecked = false
        binding.included.ultrasoundRadioBtn.isChecked = false
    }
}
