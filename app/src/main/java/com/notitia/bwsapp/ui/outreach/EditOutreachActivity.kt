package com.notitia.bwsapp.ui.outreach

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityEditOutreachBinding
import com.notitia.bwsapp.ui.profile.ProfileOutreachActivity
import com.notitia.bwsapp.util.*
import com.notitia.bwsapp.util.SetDate
import kotlinx.android.synthetic.main.activity_edit_outreach.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class EditOutreachActivity : AppCompatActivity(),KodeinAware,OutreachListener {
    override val kodein by kodein()

    private val factory: OutreachViewModelFactory by instance<OutreachViewModelFactory>()
    private lateinit var viewmodel: OutreachViewModel
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    var outreachId:String? = null
    var profileCount:Int = 0
    var outreach:Outreach? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Edit Outreach Program"

        val binding:ActivityEditOutreachBinding = DataBindingUtil.setContentView(this,R.layout.activity_edit_outreach)
        viewmodel = ViewModelProvider(this,factory).get(OutreachViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.outreachListener = this

        SetDate(binding.startDateEdt,null,this)
        SetDate(binding.endDateEdt,null,this)

        viewmodel.program.observe(this, Observer {
            if(it!=null){
                outreach = it
                outreachId = it.uniqueId
                startDateEdt.setText(SimpleDateFormat(myFormat, Locale.UK).format(it.startDate!!))
                endDateEdt.setText(SimpleDateFormat(myFormat, Locale.UK).format(it.endDate!!))
            }
        })
        viewmodel.outreachProfile.observe(this, Observer {
            profileCount = it
        })

        binding.saveOutreachButton.setOnClickListener{
            viewmodel.title = binding.nameEdt.text.toString()
            viewmodel.location = binding.locationEdt.text.toString()
            viewmodel.maleAttendance = binding.maleAttendanceEdt.text.toString()
            viewmodel.femaleAttendance = binding.femaleAttendanceEdt.text.toString()
            viewmodel.awareness = binding.awarenessEdt.text.toString()
            viewmodel.startDate = binding.startDateEdt.text.toString()
            viewmodel.endDate = binding.endDateEdt.text.toString()
            viewmodel.cervical = binding.cervicalCancerRadioBtn.isChecked
            viewmodel.breast = binding.breastCancerRadioBtn.isChecked
            viewmodel.prostate = binding.prostateRadioBtn.isChecked
            viewmodel.utrasound = binding.ultrasoundRadioBtn.isChecked
            if(viewmodel.updateOutreach()) {
                finish()
            }else{
                toast("Cannot submit empty fields")
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.ic_menu_editoutreach, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        if(id == R.id.action_person){
            prefs.savePreference(EditOutreach,outreachId)
            Intent(this,ProfileOutreachActivity::class.java).also {
                startActivity(it)
                finish()
            }
        }
        /*if(id == R.id.action_delete){
            prefs.savePreference(EditOutreach,outreachId)
            if(profileCount>0) {
                editOut.snackbar("Cannot delete this outreach")
            }else{
                val dialog = AlertDialog.Builder(this)
                dialog.setTitle("Delete Outreach Program")
                dialog.setMessage("Are you sure you want to delete this item")
                dialog.setCancelable(false)
                dialog.setPositiveButton("Delete",DialogInterface.OnClickListener { dialog, which ->
                    viewmodel.deleteOutreachLocal(outreach!!)
                    dialog.dismiss()
                })
                dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
                    .show()
                // write delete code
            }
        }*/
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        toast(message)
        finish()
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>) {

    }
}
