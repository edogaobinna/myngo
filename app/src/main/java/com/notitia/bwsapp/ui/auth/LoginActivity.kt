package com.notitia.bwsapp.ui.auth

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityLoginBinding
import com.notitia.bwsapp.ui.home.HomeActivity
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : AppCompatActivity(),KodeinAware,AuthListener{
    override val kodein by kodein()

    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: AuthViewModel
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        mAuth = FirebaseAuth.getInstance()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProvider(this,factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.authListener = this

        viewmodel.knowSignin.observe(this, Observer {
            if(!it.isNullOrEmpty()){
                binding.user.setText(it)
            }
        })

    }

    override fun onStarted() {
        login.hide()
        progress_bar.show()
    }

    override fun onError(message: String) {
        toast(message)
    }

    override fun onSuccess(message: String) {
        //toast(message)
        Intent(this,HomeActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    override fun onStopped() {
        login.show()
        progress_bar.hide()
    }

    override fun onForgetDisplay() {
        val view = LayoutInflater.from(this).inflate(R.layout.reset_password, null) as View
        val editText:EditText = view.findViewById(R.id.resetPassword)
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Forgot Password")
        dialog.setView(view)
        dialog.setCancelable(false)
        dialog.setPositiveButton("Reset", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            viewmodel.resetPassword(editText.text.toString())
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, i: Int) {
        if(i==1) {
            response.observe(this, Observer {
                if (it as Boolean) {
                    viewmodel.userDetails()
                } else {
                    onError("The email and password combination does not exist")
                    onStopped()
                }
            })
        }else if(i==3){
            response.observe(this, Observer {
                if(it as Boolean){
                    if(it){
                        onError("Email Sent")
                    }else{
                        onError("The email does not exist on record.")
                    }
                }else{
                    onError("Email was not sent. Check network connection")
                }
            })
        }
        else{
            response.observe(this, Observer {
                viewmodel.loginUser(it as String)
            })
        }
    }

}
