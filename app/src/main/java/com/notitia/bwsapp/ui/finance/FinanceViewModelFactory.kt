package com.notitia.bwsapp.ui.finance

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

class FinanceViewModelFactory (
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return FinanceViewModel(repository,prefs) as T
        }
}