package com.notitia.bwsapp.ui.profile

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityProfileDetailBinding
import com.notitia.bwsapp.util.*
import com.notitia.bwsapp.util.SetDate
import kotlinx.android.synthetic.main.activity_profile_detail.*
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.util.*

class ProfileDetailActivity : AppCompatActivity(),KodeinAware,ProfileListener {

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: ProfileViewModel
    private lateinit var myCalendar: Calendar
    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Profile"

        val binding: ActivityProfileDetailBinding = DataBindingUtil.setContentView(this,R.layout.activity_profile_detail)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.profileListener = this

        viewmodel.singleProfile.observe(this, Observer {
            if(it!=null){
                if(it.gender.equals("male",true)) {
                    patientParity.text = "No value for parity because of gender"
                }else{
                    patientParity.text = it.parity
                }
            }
        })

        viewmodel.singleMed.observe(this, Observer {
            if(it!=null){
                val bmi =  if(it.BMI.isNullOrEmpty()) "NIL" else {
                    it.BMI
                }
                patientMedHistory.text = "The patient has a BP value of ${it.systolicBP}/${it.diastolicBP} mmHg " +
                        "with BMI value of $bmi"
            }else{
                patientMedHistory.text = "No medical result for this patient"
            }
        })

        viewmodel.singleTest.observe(this, Observer {
            if(it!=null){
                val date = DateFormat.getDateInstance(DateFormat.MEDIUM)
                    .format(it.dateConducted!!)
                patientScreening.text = "The patient test result showed ${it.testResults} for ${it.testTypes} which was taken on " +
                        "$date at ${it.venueConducted}"
            }else{
                patientScreening.text = "No screening result for this patient"
            }
        })

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.ic_menu_profile, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        if(id==R.id.edit){
            viewmodel.editScreening()
            return true
        }
        if (id == R.id.delete) {
            if(!prefs.getLastSavedAt(roleId).equals("admin",true)){
                AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("You cannot perform this action due to account type")
                    .setCancelable(false)
                    .setPositiveButton("OK") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setNegativeButton(
                        android.R.string.no
                    ) { dialogInterface, _ -> dialogInterface.dismiss() }.show()
            }else {
                AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("Are you sure you want to delete this profile?")
                    .setCancelable(false)
                    .setPositiveButton("Yes") { _, _ ->
                        viewmodel.deleteProfile()
                        finish()
                    }
                    .setNegativeButton(
                        android.R.string.no
                    ) { dialogInterface, _ -> dialogInterface.dismiss() }.show()
            }
            return true
        }
        if(id == R.id.add){
            Intent(this,NewTestActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id == R.id.test){
            Intent(this,ViewResultActivity::class.java).also {
                startActivity(it)
            }
            return true
        }
        if(id==R.id.refresh){
            val view = LayoutInflater.from(this).inflate(R.layout.download_result, null) as View
            alertDialog = AlertDialog.Builder(this)
            alertDialog?.setView(view)
            alertDialog?.setCancelable(false)
            alert = alertDialog?.create()
            alert?.show()
            viewmodel.getResults(1)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        alert?.dismiss()
        Toast.makeText(this,"Download Completed", Toast.LENGTH_LONG).show()
    }

    override fun onSuccess(message: String) {
        val obj = JSONObject(message)
        val view = LayoutInflater.from(this).inflate(R.layout.screening_item, null) as View
        val screening: TextView = view.findViewById(R.id.screening)
        val test: AppCompatEditText = view.findViewById(R.id.testDateEdt)
        myCalendar = SetDate(test,null,this).getCalendar()
        screening.text = Converter().timeStamptoDate(obj.getString(DateConducted)).toString()
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Update Screening Test Date")
        dialog.setView(view)
        dialog.setCancelable(false)
        dialog.setPositiveButton("Update", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            viewmodel.updateScreening(myCalendar)
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel", DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }

    override fun onError(message: String) {
        toast(message)
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        if(id==1) {
            response.observe(this, androidx.lifecycle.Observer {
                if ((it as QuerySnapshot).isEmpty) {
                    onError("Screening Data is Empty")
                } else {
                    viewmodel.processScreening(it)
                }
            })
        }
        if(id==2){
            response.observe(this, androidx.lifecycle.Observer {
                if ((it as QuerySnapshot).isEmpty) {
                    viewmodel.getResults(2)
                } else {
                    viewmodel.processResults(it,1)
                }
            })
        }
        if(id==3){
            response.observe(this, androidx.lifecycle.Observer {
                if ((it as QuerySnapshot).isEmpty) {
                    viewmodel.getResults(3)
                } else {
                    viewmodel.processResults(it,2)
                }
            })
        }
        if(id==4){
            response.observe(this, androidx.lifecycle.Observer {
                if ((it as QuerySnapshot).isEmpty) {
                    onStarted()
                } else {
                    viewmodel.processResults(it,3)
                }
            })
        }
    }
}
