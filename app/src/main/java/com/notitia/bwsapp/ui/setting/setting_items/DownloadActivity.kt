package com.notitia.bwsapp.ui.setting.setting_items

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.ActivityDownloadBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.ui.setting.SettingViewModel
import com.notitia.bwsapp.ui.setting.SettingViewModelFactory
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.initialize_layout.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DownloadActivity : AppCompatActivity(), KodeinAware, SetListener {

    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel
    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Export Database Records"

        val binding: ActivityDownloadBinding = DataBindingUtil.setContentView(this,R.layout.activity_download)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.setListener = this
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int) {
        val view = LayoutInflater.from(this).inflate(R.layout.initialize_layout, null) as View
        view.name.hide()
        view.synctext?.text = "Please Wait while we export records"
        alertDialog = AlertDialog.Builder(this)
        alertDialog?.setView(view)
        alertDialog?.setCancelable(false)
        alert = alertDialog?.create()
        alert?.show()
        if(id==1){
            viewmodel.profiles.observe(this, Observer {
                if(it.isNotEmpty()) {
                    viewmodel.exportProfileToCsv(it, this)
                }
            })
        }
        if(id==2){
            viewmodel.medicals.observe(this, Observer {
                if(it.isNotEmpty()) {
                    viewmodel.exportMedicalToCsv(it, this)
                }
            })
        }
        if(id==3){
            viewmodel.outreach.observe(this, Observer {
                if(it.isNotEmpty()) {
                    viewmodel.exportOutreachToCsv(it, this)
                }
            })
        }
        if(id==4){
            viewmodel.screening.observe(this, Observer {
                if(it.isNotEmpty()) {
                    viewmodel.exportScreeningToCsv(it, this)
                }
            })
        }
    }

    override fun onSuccess(message: String) {
        alert?.dismiss()
        toast(message)
    }

    override fun onError(message: String) {
        alert?.dismiss()
        toast(message)
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {

    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
