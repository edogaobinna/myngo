package com.notitia.bwsapp.ui.profile.stepper.investigation

interface InvestListener {
    fun onStarted()
    fun onSuccess(message:String)
    fun onError(message:String)
}