package com.notitia.bwsapp.ui.setting.setting_items

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityUpdateBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.ui.setting.SettingViewModel
import com.notitia.bwsapp.ui.setting.SettingViewModelFactory
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_update.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class UpdateActivity : AppCompatActivity(),KodeinAware,SetListener {
    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: SettingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Check Application Update"

        val binding: ActivityUpdateBinding = DataBindingUtil.setContentView(this, R.layout.activity_update)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.setListener = this

        viewmodel.getUpdate()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int) {
        if(id==1) {
            progress_bar.hide()
            check.hide()
            updateValue.text = "Hooray!!, Application is up-to-date"
            updateValue.show()
        }else{
            toast("Downloading update package.....")
            val downloadManager =
                getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val uri: Uri = Uri.parse(prefs.getLastSavedAt("url"))
            val request = DownloadManager.Request(uri)
            request.setVisibleInDownloadsUi(true)
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                "myNGO.apk"
            )
            downloadManager.enqueue(request)
        }
    }

    override fun onSuccess(message: String) {
        progress_bar.hide()
        check.hide()
        updateValue.text = "Version $message is available for update"
        updateValue.show()
        create.show()
        noted.show()
    }

    override fun onError(message: String) {
        progress_bar.hide()
        check.hide()
        updateValue.text = "Oops!!, Seems there is an error checking for updates "
        updateValue.show()
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            viewmodel.processUpdate(it as QuerySnapshot)
        })
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {

    }
}
