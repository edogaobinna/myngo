package com.notitia.bwsapp.ui.finance.tab_items

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import org.kodein.di.android.x.kodein
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.CustomAdapter
import com.notitia.bwsapp.adapter.IncomeAdapter
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.databinding.IncomeFragmentBinding
import com.notitia.bwsapp.util.*
import kotlinx.android.synthetic.main.add_income.*
import kotlinx.android.synthetic.main.income_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class IncomeFragment : Fragment(),KodeinAware,TabListener {

    private lateinit var binding: IncomeFragmentBinding

    override val kodein by kodein()

    private val factory: IncomeViewModelFactory by instance()
    private val repository:DataRepository by instance()
    private var listPatient:ArrayList<Income>? = ArrayList()
    private var incomeAdapter: IncomeAdapter? = IncomeAdapter(listPatient)

    var years = ArrayList<String>()

    companion object {
        fun newInstance() = IncomeFragment()
    }

    private lateinit var viewModel: IncomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.income_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this,factory).get(IncomeViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel.tabListener = this

        viewModel.incomes.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                incomeAdapter?.notifyDataSetChanged()
                emptyState.hide()
                recycler_view.show()
            }else{
                emptyState.show()
                recycler_view.hide()
            }
        })

        viewModel.incomeCate.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()) {
                years = viewModel.getIncome(it)
            }
        })

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = incomeAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    recycler_view,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]
                                Utility().editIncomeDialog(context!!,repository,patient,years)
                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }

    }

    override fun onStarted(repository: DataRepository) {
        Utility().incomeDialog(context!!,repository,years)
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
