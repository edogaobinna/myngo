package com.notitia.bwsapp.ui.setting.stepper.syncfinance


import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot

import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.FragmentSyncFinanceBinding
import com.notitia.bwsapp.databinding.SyncProfileFragmentBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.ui.setting.stepper.syncprofile.SyncProfileViewModel
import com.notitia.bwsapp.ui.setting.stepper.syncprofile.SyncProfileViewModelFactory
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_sync_finance.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.lang.Exception
import kotlin.check

/**
 * A simple [Fragment] subclass.
 */
class SyncFinanceFragment : Fragment(),KodeinAware,BlockingStep,SetListener {
    private lateinit var binding: FragmentSyncFinanceBinding

    override val kodein by kodein()
    private lateinit var viewModel: SyncProfileViewModel

    private val factory: SyncProfileViewModelFactory by instance<SyncProfileViewModelFactory>()

    var count:Int = 0
    var complete:Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sync_finance, container, false)
        viewModel = ViewModelProvider(this, factory).get(SyncProfileViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        viewModel.setListener = this

        viewModel.count.observe(viewLifecycleOwner, Observer {
            count = it
        })

        viewModel.outreachSync()
        return binding.root
    }

    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback?) {
        callback?.goToPrevStep()
    }

    override fun onSelected() {
        return
    }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback?) {
        if(complete){
            callback?.stepperLayout?.showProgress("Please Wait")
            Handler().postDelayed({
                callback?.complete()
                callback?.stepperLayout?.hideProgress()
                activity?.finish()
            },3000)

        }else{
            Toast.makeText(context,"Wait while download completes", Toast.LENGTH_LONG).show()
        }
    }

    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun verifyStep(): VerificationError? {
        return null
    }

    override fun onError(error: VerificationError) {

    }

    override fun onStarted(id: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        Toast.makeText(context,"Outreach download completed click next to continue", Toast.LENGTH_LONG).show()
        check.text = "Outreach download completed click complete to finish"
        check.show()
        progress_bar.hide()
        complete = true
    }

    override fun onError(message: String) {
        Toast.makeText(context," No Outreach to download, click next to continue", Toast.LENGTH_LONG).show()
        check.text = "Click next to continue"
        check.show()
        progress_bar.hide()
        complete = true
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            if((it as QuerySnapshot).isEmpty) {
                onError("Outreach Data is Empty")
            }else{
                viewModel.processOutreach(it)
            }
        })
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
