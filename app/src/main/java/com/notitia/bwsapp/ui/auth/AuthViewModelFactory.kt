package com.notitia.bwsapp.ui.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

@Suppress("UNCHECKED_CAST")
class AuthViewModelFactory (
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AuthViewModel(repository,prefs) as T
        }
}