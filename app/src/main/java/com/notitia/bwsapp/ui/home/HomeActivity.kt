package com.notitia.bwsapp.ui.home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityHomeBinding
import com.notitia.bwsapp.ui.finance.FinanceActivity
import com.notitia.bwsapp.ui.notification.NotificationActivity
import com.notitia.bwsapp.ui.outreach.OutreachActivity
import com.notitia.bwsapp.ui.profile.ProfileActivity
import com.notitia.bwsapp.ui.setting.SettingActivity
import com.notitia.bwsapp.util.myFormat2
import com.notitia.bwsapp.util.roleId
import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity(),KodeinAware,HomeListener,View.OnClickListener {

    override val kodein by kodein()

    private val factory: HomeViewModelFactory by instance<HomeViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: HomeViewModel

    private var sheetBehavior:BottomSheetBehavior<View>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityHomeBinding = DataBindingUtil.setContentView(this,R.layout.activity_home)
        viewmodel = ViewModelProvider(this,factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.homeListener = this

        viewmodel.insertIncome()

        binding.included.profileClick.setOnClickListener(this)
        binding.included.outreach.setOnClickListener(this)
        binding.included.finance.setOnClickListener(this)
        binding.included.settings.setOnClickListener(this)

        val sdf = SimpleDateFormat(myFormat2, Locale.UK)

        binding.included.bottomClick.setOnClickListener{
            if (sheetBehavior!!.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior!!.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                sheetBehavior!!.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }

        sheetBehavior = BottomSheetBehavior.from(binding.included.sheetBottom)
        sheetBehavior!!.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })

        viewmodel.profile.observe(this, Observer {
            profiles.text = it.toString()
        })
        viewmodel.outreach.observe(this, Observer {
            outreach.text = it.toString()
        })
        viewmodel.male.observe(this, Observer {
            males.text = it.toString()
        })
        viewmodel.female.observe(this, Observer {
            females.text = it.toString()
        })
        viewmodel.user.observe(this, Observer {
            if(!it.isNullOrEmpty())
            {
                lastLoginText.text = "Last Login: ${sdf.format(it[0].lastlogin)}"
            }
        })
    }

    override fun onSuccess(id: Int?) {
        when (id) {
            1 -> Intent(this, ProfileActivity::class.java).also {
                startActivity(it)
            }
            2 -> Intent(this, OutreachActivity::class.java).also {
                startActivity(it)
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.ic_menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }

        if(id == R.id.action_notifications){
            Intent(this,NotificationActivity::class.java).also {
                startActivity(it)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.profileClick-> Intent(this,ProfileActivity::class.java).also {
                startActivity(it)
            }
            R.id.outreach->Intent(this, OutreachActivity::class.java).also {
                startActivity(it)
            }
            R.id.finance->{
                if(prefs.getLastSavedAt(roleId).equals("User",true)){
                    AlertDialog.Builder(this)
                        .setTitle("Authorization Required")
                        .setMessage("Proper clearance required to access this area!!")
                        .setCancelable(true)
                        .setPositiveButton("Ok") { dialog, _ ->
                            dialog.dismiss()
                        }.show()
                }else{
                    Intent(this, FinanceActivity::class.java).also {
                        startActivity(it)
                    }
                }
            }
            R.id.settings->Intent(this, SettingActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}
