package com.notitia.bwsapp.ui.setting.stepper.syncprofile

import android.view.View
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.db.entities.Profile
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.util.*
import org.json.JSONObject

class SyncProfileViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
) : ViewModel() {
    val profiles = repository.getProfile()
    val count = repository.getProfileCount()

    var setListener:SetListener? = null

    fun onContinueClick(view: View){
        prefs.saveBooleanPreference("Sync",true)
        setListener?.onStarted(1)
    }

    fun profileSync(){
        val response = Firebase().getDocument(Profile)
        setListener?.onFirebaseFinish(response,0)
    }

    fun outreachSync(){
        val response = Firebase().getDocument(Outreach)
        setListener?.onFirebaseFinish(response,0)
    }

    fun processData(data: QuerySnapshot){
        try {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Profile(
                            null,
                            obj.getString(UniqueId),
                            obj.getString(Surname),
                            obj.getString(firstName),
                            obj.getString(Age),
                            obj.getString(Gender),
                            obj.optString(Parity),
                            obj.getString(PhoneNumber),
                            obj.getString(Address),
                            obj.getString(Tribe),
                            obj.getString(Nationality),
                            null,
                            Converter().timeStamptoDate(obj.getString(DateCreated)),
                            obj.getBoolean(Active)
                        )
                    Coroutines.main {
                        repository.checkSaveProfile(profile)
                    }
                }
                setListener?.onSuccess("Profile download completed click next to continue")
            } else {
                setListener?.onError("")
            }
        }catch (e:Exception){
            setListener?.onError("Oops!! An error just occurred. Please Try Again")
        }
    }

    fun processOutreach(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val profile =
                    Outreach(null,
                        obj.getString(UniqueId),
                        obj.getString(Title),
                        obj.getString(Location),
                        obj.getString(MaleAttendance),
                        obj.getString(FemaleAttendance),
                        obj.getString(Awareness),
                        obj.getBoolean(BreastCancer),
                        obj.getBoolean(CervicalCancer),
                        obj.getBoolean(ProstateCancer),
                        obj.getBoolean(Ultrasound),
                        Converter().timeStamptoDate(obj.getString(StartDate)),
                        Converter().timeStamptoDate(obj.getString(EndDate)))
                Coroutines.main{
                    repository.checkSaveOutreach(profile)
                }
            }
            setListener?.onSuccess("")
        }else{
            setListener?.onError("")
        }
    }
}
