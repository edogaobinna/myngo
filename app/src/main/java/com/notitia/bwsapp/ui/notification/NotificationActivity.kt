package com.notitia.bwsapp.ui.notification

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.NotifyAdapter
import com.notitia.bwsapp.adapter.ProfileAdapter
import com.notitia.bwsapp.data.db.entities.NotProf
import com.notitia.bwsapp.data.db.entities.Notification
import com.notitia.bwsapp.data.db.entities.Profile
import com.notitia.bwsapp.databinding.ActivityNotificationBinding
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_notification.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class NotificationActivity : AppCompatActivity(),KodeinAware {
    override val kodein by kodein()
    private val factory: NotifyViewModelFactory by instance()

    private lateinit var viewmodel: NotificationViewModel
    private var listPatient:ArrayList<NotProf>? = ArrayList()
    private var profileAdapter: NotifyAdapter? = NotifyAdapter(listPatient)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Notifications"

        val binding: ActivityNotificationBinding = DataBindingUtil.setContentView(this,R.layout.activity_notification)
        viewmodel = ViewModelProvider(this,factory).get(NotificationViewModel::class.java)
        binding.viewmodel = viewmodel

        viewmodel.notifications.observe(this, Observer {
            if(it.isNotEmpty()){
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                profileAdapter?.notifyDataSetChanged()
                emptyState.hide()
                recycler_view.show()
            }else{
                emptyState.show()
                recycler_view.hide()
            }
        })

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = profileAdapter
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
