package com.notitia.bwsapp.ui.profile

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.notitia.bwsapp.data.db.entities.*
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ProfileViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    val profile = repository.getProfile()
    val outreachProfile = repository.getProfileWithOutreach()
    val tests = repository.getTests()

    val singleProfile = repository.getSingleProfile()
    val singleTest = repository.getSingleScreen()
    val singleMed = repository.getSingleMed()

    val nullProfile = repository.getNullOutreach()

    private val _role = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(roleId)
    }

    val knowRole: LiveData<String?> = _role


    var papSmear:String? = null
    var via:String? = null
    var cryo:String? = "Done"
    var cryoBool:Boolean? = false
    var psa:String? = null
    var mammo:String? = null
    var ultra:String? = null

    var paprefer:Boolean? = null
    var papdescription:String? = null
    var viarefer:Boolean? = null
    var viadescription:String? = null
    var psarefer:Boolean? = null
    var psadescription:String? = null
    var mamrefer:Boolean? = null
    var mamdescription:String? = null
    var ultrarefer:Boolean? = null
    var ultradescription:String? = null

    var profileListener:ProfileListener? = null

    fun onAddProfileClick(view: View){
        profileListener?.onStarted()
    }

    fun deleteProfile(){
        Coroutines.main{
            val profile = singleProfile.value!!
            profile.active = true
            repository.saveProfile(profile)

            Firebase().deleteProfile(profile.uniqueId!!)
        }
    }

    fun addNewTest(view:View){
        if(!papSmear.isNullOrEmpty() || !cryo.isNullOrEmpty() || !psa.isNullOrEmpty() || !mammo.isNullOrEmpty()
            || !ultra.isNullOrEmpty()) {

            val uniqueId = Utility().getUniqueID()

            if(!cryoBool!!){
                cryo = "Not Done"
            }

            val medical = Tests(
                null,
                prefs.getLastSavedAt(UniqueId)!!,
                uniqueId,
                papSmear,
                PapReferred(paprefer,papdescription),
                cryo,
                psa,
                PsaReferred(psarefer,psadescription),
                mammo,
                MamReferred(mamrefer,mamdescription),
                ultra,
                UltraReferred(ultrarefer,ultradescription)
            )

            Coroutines.main {
                repository.saveTest(medical)
            }
            val medicals = HashMap<String, Any?>()
            medicals[UniqueId] = medical.uniqueId
            medicals[ProfileId] = medical.profileId
            medicals[PapSmear] = medical.papsmearResult
            medicals[PapReferred] = medical.papsReferred!!.referred
            medicals[PapDescription] = medical.papsReferred!!.description


            medicals[CryotherapyResult] = medical.cryotherapyResult
            medicals[PsaResult] = medical.psaResult
            medicals[PsaReferred] = medical.psaReferred!!.referred
            medicals[PsaDescription] = medical.psaReferred!!.description

            medicals[MammographyResult] = medical.mammographyResult
            medicals[MamReferred] = medical.mamReferred!!.referred
            medicals[MamDescription] = medical.mamReferred!!.description

            medicals[UltrasoundResult] = medical.ultrasoundResult
            medicals[UltraReferred] = medical.ultraReferred!!.referred
            medicals[UltraDescription] = medical.ultraReferred!!.description

            Firebase().createMedicalDocument(Test, UniqueId, uniqueId!!, medicals)

            profileListener?.onSuccess("Test Added Successfully")
        }
    }

    fun getPatients(){
        val response = Firebase().getDocumentIdentifier(Profile,prefs.getLastSavedAt(EditOutreach)!!,
            OutreachId)
        profileListener?.onFirebaseFinish(response,1)
    }

    fun processPatients(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val profile =
                    Profile(null,obj.getString(UniqueId),
                    obj.getString(Surname),obj.getString(firstName),obj.getString(Age),
                    obj.getString(Gender),obj.optString(Parity),obj.getString(PhoneNumber),
                    obj.getString(Address),obj.optString(Tribe),obj.getString(Nationality),obj.getString(
                            OutreachId),Converter().timeStamptoDate(obj.getString(DateCreated)),obj.getBoolean(Active))
                Coroutines.main{
                    repository.checkSaveProfile(profile)
                }
            }
            profileListener?.onSuccess("")
        }else{
            profileListener?.onError("")
        }
    }

    fun editScreening(){
        val response = Firebase().getDocumentIdentifier(Screening, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
        profileListener?.onFirebaseFinish(response,1)
    }

    fun processScreening(data: QuerySnapshot){
        if(!data.isEmpty) {
            var document:String? = null
            data.forEach {
                document = Gson().toJson(it.data)
            }
            profileListener?.onSuccess(document!!)
        }else{
            profileListener?.onError("End")
        }
    }

    fun updateScreening(myCalendar:Calendar){
        val date = Date(myCalendar.timeInMillis)
        Firebase().updateScreening(prefs.getLastSavedAt(UniqueId)!!,date)
    }

    fun getResults(id:Int){
        if(id==1) {
            val response = Firebase().getDocumentIdentifier(Medical, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
            profileListener?.onFirebaseFinish(response, 2)
        }
        if(id==2){
            val response = Firebase().getDocumentIdentifier(Screening, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
            profileListener?.onFirebaseFinish(response,3)
        }
        if(id==3){
            val response = Firebase().getDocumentIdentifier(Test, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
            profileListener?.onFirebaseFinish(response,4)
        }
    }

    fun processResults(data: QuerySnapshot,id:Int){
        if(id==1) {
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Medical(
                            null,obj.getString(ProfileId),obj.getString(UniqueId),obj.optString(
                                Systolic),obj.optString(Diastolic),obj.optString(BloodSugar),obj.optString(
                                Height),obj.optString(Weight),obj.optString(BMI)
                        )
                    Coroutines.main {
                        repository.checkSaveMedical(profile)
                    }
                }
                val response = Firebase().getDocumentIdentifier(Screening, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
                profileListener?.onFirebaseFinish(response,3)
            } else {
                val response = Firebase().getDocumentIdentifier(Screening, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
                profileListener?.onFirebaseFinish(response,3)
                //profileListener?.onError("Error Occurred, Check Internet connection and try again")
            }
        }
        if(id==2){
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Screening(
                            null,obj.getString(ProfileId),obj.getString(UniqueId),obj.optString(
                                TestType),obj.optString(TestResult),Converter().timeStamptoDate(obj.getString(DateConducted)),
                            obj.optString(VenueConducted),obj.optString(ReportingDoctor)
                        )
                    Coroutines.main {
                        repository.checkSaveScreening(profile)
                    }
                }
                val response = Firebase().getDocumentIdentifier(Test, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
                profileListener?.onFirebaseFinish(response,4)
            } else {
                val response = Firebase().getDocumentIdentifier(Test, prefs.getLastSavedAt(UniqueId)!!, ProfileId)
                profileListener?.onFirebaseFinish(response,4)
                //profileListener?.onError("Error Occurred, Check Internet connection and try again")
            }
        }
        if(id==3){
            if (!data.isEmpty) {
                data.forEach {
                    val document = Gson().toJson(it.data)
                    val obj = JSONObject(document)
                    val profile =
                        Tests(
                            null,obj.getString(ProfileId),obj.getString(UniqueId),obj.optString(
                                PapSmear),PapReferred(obj.optBoolean(PapReferred),obj.optString(
                                PapDescription)),obj.optString(CryotherapyResult),obj.optString(
                                PsaResult), PsaReferred(obj.optBoolean(PsaReferred),obj.optString(
                                PsaDescription)),obj.optString(MammographyResult), MamReferred(obj.optBoolean(
                                MamReferred),obj.optString(MamDescription)),obj.optString(
                                UltrasoundResult), UltraReferred(obj.optBoolean(UltraReferred),obj.optString(
                                UltraDescription))
                        )
                    Coroutines.main {
                        repository.checkSaveTest(profile)
                    }
                }
                profileListener?.onStarted()
            } else {
                profileListener?.onStarted()
                //profileListener?.onError("Error Occurred, Check Internet connection and try again")
            }
        }
    }
}