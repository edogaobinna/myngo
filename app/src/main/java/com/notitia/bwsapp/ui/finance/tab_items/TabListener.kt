package com.notitia.bwsapp.ui.finance.tab_items

import com.notitia.bwsapp.data.repositories.DataRepository

interface TabListener {
    fun onStarted(repository: DataRepository)
    fun onSuccess(message:String)
    fun onError(message:String)
}