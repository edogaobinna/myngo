package com.notitia.bwsapp.ui.setting.setting_items

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.Stepper1Adapter
import com.notitia.bwsapp.adapter.StepperAdapter
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.util.toast
import com.stepstone.stepper.StepperLayout
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SyncActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()
    private var mStepperLayout: StepperLayout? = null

    private val prefs:PreferenceProvider by instance<PreferenceProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Synchronize Data"
        setContentView(R.layout.activity_sync)

        mStepperLayout = findViewById(R.id.stepperLayout)
        mStepperLayout?.adapter = Stepper1Adapter(this.supportFragmentManager, this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            if(prefs.getLastBoolean("Sync")!!){
                toast("Synchronization cannot be stopped, please allow to finish")
            }else{
                finish()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if(prefs.getLastBoolean("Sync")!!){
            toast("Synchronization cannot be stopped, please allow to finish")
        }else{
            super.onBackPressed()
            finish()
        }
    }
}
