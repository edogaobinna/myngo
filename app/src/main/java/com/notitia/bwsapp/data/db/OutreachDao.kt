package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.db.entities.Profile
import java.util.*

@Dao
interface OutreachDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(outreach: Outreach) : Long
    @Delete
    suspend fun delete(outreach:Outreach):Int

    @Query("UPDATE outreach SET startDate = :start, endDate = :end, male_attendance=:male,female_attendance=:female WHERE uniqueId=:uid")
    suspend fun updateOutreach(uid:String?,start: Date?,end:Date?,male:String?,female:String?)

    @Query("SELECT * FROM outreach ORDER BY id DESC")
    fun getOutreachs() : LiveData<List<Outreach>>

    @Query("SELECT COUNT(*) FROM outreach")
    fun getOutreachCount() : LiveData<Int>

    @Query("SELECT * FROM outreach WHERE id=:uid")
    fun getSingleOutreach(uid:Long) : LiveData<Outreach>

    @Query("SELECT COUNT(*) FROM outreach WHERE uniqueId = :uid")
    suspend fun getOneOutreach(uid:String?) : Int
}