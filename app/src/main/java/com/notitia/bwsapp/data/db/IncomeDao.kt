package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Income

@Dao
interface IncomeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(income: Income) : Long

    @Query("SELECT * FROM income ORDER BY id DESC")
    fun getInomes() : LiveData<List<Income>>

}