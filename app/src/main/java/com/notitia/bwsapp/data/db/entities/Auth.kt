package com.notitia.bwsapp.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "auth")
data class Auth (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var name: String? = null,
    var employeeId:String? = null,
    var username:String? = null,
    var lastlogin: Date? = null,
    var role: String? = null,
    var activated:Boolean? = null
)