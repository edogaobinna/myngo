package com.notitia.bwsapp.data.repositories

import com.notitia.bwsapp.data.db.AppDatabase
import com.notitia.bwsapp.data.db.entities.*
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.util.EditOutreach
import com.notitia.bwsapp.util.Outreach
import com.notitia.bwsapp.util.UniqueId
import java.lang.Exception
import java.util.*

class DataRepository(
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
) {
    suspend fun checkSaveProfile(profile:Profile){
        val itemFromDB: Int = getOneProfile(profile.uniqueId!!)
        if (itemFromDB==0) {
            saveProfile(profile)
        }
    }
    suspend fun checkSaveMedical(medical: Medical){
        val itemFromDB: Int = getOneMedical(medical.uniqueId!!)
        if (itemFromDB==0) {
            saveMedical(medical)
        }
    }
    suspend fun checkSaveScreening(screening: Screening){
        val itemFromDB: Int = getOneScreening(screening.uniqueId!!)
        if (itemFromDB==0) {
            saveScreening(screening)
        }
    }
    suspend fun checkSaveTest(tests: Tests){
        val itemFromDB: Int = getOneTest(tests.uniqueId!!)
        if (itemFromDB==0) {
            saveTest(tests)
        }
    }

    suspend fun checkSaveOutreach(outreach: Outreach){
        try {
            val itemFromDB: Int = getOneOutreach(outreach.uniqueId!!)
            if (itemFromDB==0) {
                saveOutreach(outreach)
            }
        }catch (e:Exception){
            //saveOutreach(outreach)
        }
    }

    suspend fun checkSaveCate(category: Category){
        try {
            val itemFromDB: Int = getOneCate(category.Type!!)
            if (itemFromDB==0) {
                saveCategory(category)
            }
        }catch (e:Exception){
            //saveCategory(category)
        }
    }

    suspend fun saveAuth(auth: Auth) = db.getAuthDao().insert(auth)
    suspend fun saveIncome(income: Income) = db.getIncomeDao().insert(income)
    suspend fun saveProfile(profile: Profile) = db.getProfileDao().insert(profile)
    suspend fun saveExpense(expense: Expense) = db.getExpenseDao().insert(expense)
    suspend fun saveOutreach(outreach: Outreach) = db.getOutreachDao().insert(outreach)
    suspend fun saveMedical(medical: Medical) = db.getMedicalDao().insert(medical)
    suspend fun saveScreening(screening: Screening) = db.getScreeningDao().insert(screening)
    suspend fun saveTest(tests: Tests) = db.getTestDao().insert(tests)
    suspend fun saveCategory(category: Category) = db.getCategoryDao().insert(category)
    suspend fun saveNotification(notification: Notification) = db.getNotifyDao().insert(notification)
    suspend fun deleteOutreach(outreach: Outreach) = db.getOutreachDao().delete(outreach)
    suspend fun updateOutrech(id:String,start: Date,end:Date,male:String,female:String) = db.getOutreachDao().updateOutreach(id,start,end,male,female)

    fun getOutreach()= db.getOutreachDao().getOutreachs()
    fun getNotifies()= db.getNotifyDao().getNotifications()
    fun getProfile()= db.getProfileDao().getProfiles(false)
    fun getMedicals() = db.getMedicalDao().getMedicals()
    fun getSingleProfile()= db.getProfileDao().getProfile(prefs.getLastSavedAt(UniqueId))
    fun getProfileWithOutreach()= db.getProfileDao().getWithOutreach(prefs.getLastSavedAt(EditOutreach))
    fun getNullOutreach()= db.getProfileDao().getNullOutreach()
    suspend fun getOneProfile(id:String)= db.getProfileDao().getSingleProfile(id)
    suspend fun getOneMedical(id:String)= db.getMedicalDao().getOneMedical(id)
    suspend fun getOneTest(id:String)= db.getTestDao().getOneTest(id)
    suspend fun getOneScreening(id:String)= db.getScreeningDao().getOneScreening(id)
    suspend fun getOneOutreach(id:String)= db.getOutreachDao().getOneOutreach(id)
    suspend fun getOneCate(id:String)= db.getCategoryDao().getCategory(id)
    fun getSingleScreen()= db.getScreeningDao().getSingleTest(prefs.getLastSavedAt(UniqueId))
    fun getSingleMed()= db.getMedicalDao().getSingleMedical(prefs.getLastSavedAt(UniqueId))
    fun getSingleProgram()= db.getOutreachDao().getSingleOutreach(prefs.getLastSavedAt(Outreach)!!.toLong())
    fun getSingleTest()= db.getTestDao().getSingleTest(prefs.getLastSavedAt(UniqueId))
    fun getAuth() = db.getAuthDao().getUser()
    fun getIncome() = db.getIncomeDao().getInomes()
    fun getExpense() = db.getExpenseDao().getExpenses()
    fun getScreening() = db.getScreeningDao().getTest()
    fun getCategories(uid:String) = db.getCategoryDao().getCategories(uid)
    fun getTests() = db.getTestDao().getTests()

    fun getMaleCount() = db.getProfileDao().getMaleRecords("Male",false)
    fun getFemaleCount() = db.getProfileDao().getFemaleRecords("Female",false)
    fun getProfileCount() = db.getProfileDao().getPatientCount(false)
    fun getProfileOutreachCount() = db.getProfileDao().getOutreachPatientCount(false,prefs.getLastSavedAt(EditOutreach))
    fun getOutreachCount() = db.getOutreachDao().getOutreachCount()
}