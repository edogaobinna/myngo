package com.notitia.bwsapp.data.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.notitia.bwsapp.util.*
import java.util.*
import kotlin.collections.HashMap


class Firebase {
    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    fun siginIn(email:String, password:String): LiveData<Any> {
        val isSuccessful = MutableLiveData<Any>()
        mAuth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener{
               isSuccessful.value = it.isSuccessful
            }
        return isSuccessful
    }
    fun signOut(): LiveData<Any> {
        val isSuccessful = MutableLiveData<Any>()
        mAuth.signOut()
        isSuccessful.value = true
        return isSuccessful
    }

    fun sendPasswordRest(email:String):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener{
            isSuccessful.value = it.isSuccessful
        }
        return isSuccessful
    }


    fun createUser(email: String,password: String,user:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        mAuth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    db.collection(Users).add(user)
                        .addOnCompleteListener{it1->
                            if(it1.isSuccessful) {
                                isSuccessful.value = it1.isSuccessful
                            }else{
                                isSuccessful.value = false
                            }
                        }

                }else {
                    isSuccessful.value = false
                }
            }
        return isSuccessful
    }

    fun createDocument(collection:String,document:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).add(document)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    isSuccessful.value = it.isSuccessful
                }else {
                    isSuccessful.value = false
                }
            }
        return isSuccessful
    }

    fun createMedicalDocument(collection:String,field:String, identifier:String,document:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()==0){
                    db.collection(collection).add(document)
                        .addOnCompleteListener{int->
                            if(int.isSuccessful){
                                isSuccessful.value = int.isSuccessful
                            }else {
                                isSuccessful.value = false
                            }
                        }
                }else{
                    isSuccessful.value = false
                }
            }

        return isSuccessful
    }

    fun getSingleDocument(collection:String, field:String, identifier:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single()
                    data.value = Gson().toJson(document.data)
                }else{
                    data.value = ""
                }
            }
        return data
    }
    fun deleteProfile(identifier:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(Profile).whereEqualTo(UniqueId,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.update("Active",true)
                }else{
                    data.value = ""
                }
            }
        return data
    }

    fun updateScreening(identifier:String,value: Date):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(Screening).whereEqualTo(ProfileId,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.update(DateConducted,value)
                }else{
                    data.value = ""
                }
            }
        return data
    }

    fun disableUser(identifier:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(Users).whereEqualTo(user_name,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.update(activated,false)
                }else{
                    data.value = ""
                }
            }
        return data
    }

    fun updateDocument(collection:String, identifier:String,document:HashMap<String,Any?>):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(UniqueId,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val doc = it.result!!.single().reference
                    doc.update(document)
                }else{
                    data.value = ""
                }
            }
        return data
    }
    fun getDocument(collection:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    data.value = it.result
                }else{
                    data.value = it.result
                }
            }
        return data
    }

    fun getDocumentIdentifier(collection:String,identifier: String,field:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier).get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    data.value = it.result
                }else{
                    data.value = it.result
                }
            }
        return data
    }
    fun deleteOutreach(collection:String,field:String, identifier:String):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.delete()
                    isSuccessful.value = true
                }else{
                    isSuccessful.value = false
                }
            }

        return isSuccessful
    }
}