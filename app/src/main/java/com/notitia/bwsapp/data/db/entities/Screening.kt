package com.notitia.bwsapp.data.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "screening", indices = [Index(value = ["profileId"])],foreignKeys = [ForeignKey(
    entity = Profile::class,
    parentColumns = arrayOf("uniqueId"),
    childColumns = arrayOf("profileId"),
    onDelete = ForeignKey.CASCADE
)]
)
data class Screening (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var profileId:String? = null,
    var uniqueId:String? = null,
    var testTypes: String? = null,
    var testResults: String? = null,
    var dateConducted: Date? = null,
    var venueConducted: String? = null,
    var doctor: String? = null
)