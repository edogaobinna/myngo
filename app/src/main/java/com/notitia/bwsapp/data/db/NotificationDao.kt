package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.*

@Dao
interface NotificationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(notification: Notification) : Long

    @Query("SELECT notification.*, profile.* FROM notification INNER JOIN profile ON profileId=profile.uniqueId WHERE profileId=profile.uniqueId AND dueDate>=CURRENT_TIMESTAMP +3")
    fun getNotifications() : LiveData<List<NotProf>>

    @Query("SELECT * FROM notification WHERE profileId = :uid")
    fun getNotification(uid:String?) : LiveData<Notification>
}