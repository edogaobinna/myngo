package com.notitia.bwsapp.data.db.entities

import androidx.room.*
import com.notitia.bwsapp.util.DateConverter
import java.util.*

@Entity(tableName = "notification", indices = [Index(value = ["profileId"])],foreignKeys = [ForeignKey(
    entity = Profile::class,
    parentColumns = arrayOf("uniqueId"),
    childColumns = arrayOf("profileId"),
    onDelete = ForeignKey.CASCADE
)]
)
data class Notification (
    @PrimaryKey(autoGenerate = true)
    var uid: Long? = null,
    var profileId:String? = null,
    var uuniqueId:String? = null,
    var test: String? = null,
    @ColumnInfo(name = "dueDate")
    @TypeConverters(DateConverter::class)
    var duedate: Date? = null
)