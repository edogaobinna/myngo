package com.notitia.bwsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.db.entities.Profile

class UserAdapter(patients:ArrayList<Auth>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<Auth>? = patients

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.name)
        var about: TextView = view.findViewById(R.id.accountType)
        var address: TextView = view.findViewById(R.id.user)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_users, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: Auth = mRecyclerViewItems!![position]
            menuItemHolder.name.text = "${item.name}"
            menuItemHolder.about.text = "Account Type: ${item.role}"
            menuItemHolder.address.text = "${item.username}"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}