package com.notitia.bwsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Profile

class ProfileAdapter(patients:ArrayList<Profile>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<Profile>? = patients

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.profile_name)
        var about: TextView = view.findViewById(R.id.profile_about)
        var address: TextView = view.findViewById(R.id.profile_address)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.profile_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: Profile = mRecyclerViewItems!![position]
            menuItemHolder.name.text = "${item.surname} ${item.firstName}"
            menuItemHolder.about.text = "${item.gender}, ${item.age} years"
            menuItemHolder.address.text = "#- ${item.address}"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}